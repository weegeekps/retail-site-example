'use strict';

const express = require('express');
const fs = require('fs');
const path = require('path');

/*
 * A note for the reviewer.
 *
 * Normally, there is a lot more that needs to go into a Node application. This includes middleware for logging,
 * authentication, database access, etc. Since the Node application for this case study only needs to serve the
 * static content and the item-data.json provided, I've instead gone for the bare minimum amount of code here.
 */

const app = express();

// Not using require to parse this JSON as it appears to have UTF-8 characters. The require function can only parse
//  ASCII JSON files.
const string = fs.readFileSync('./item-data.json', 'utf8');
const itemData = JSON.parse(string);

// In "Prod" we want to use a "static" folder that the built Aurelia app will be copied to.
const staticFilesPath = path.join(__dirname, process.env.NODE_ENV === 'prod' ? 'static' : '../client/dist');
app.use(express.static(staticFilesPath, {index: false}));

// Only one actual route. We just serve back the item data.
app.get('/api/v1/catalog-entry/:id', function (req, res) {
  // We actually don't care about req.params.id at all. Just put it there to make this look as RESTful as possible.
  // In practice we would get catalog entry 205273068, which is what the JSON is returning.

  res.json(itemData);
});

app.get(/^\/(?!api).*/, function (req, res) {
  // Push State support.
  if (!res.headersSent) {
    res.sendFile(`${staticFilesPath}/index.html`, {}, err => console.error(err));
  }
});

const port = process.env.NODE_PORT || 3000;
app.listen(port, () => console.log(`Listening on port ${port}`));

module.exports = app;
