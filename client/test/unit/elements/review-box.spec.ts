import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';

const customerReviews = {
  'Reviews': [
    {
      'city': 'NYC',
      'customerId': '110657105',
      'datePosted': 'Mon Mar 11 13:13:55 UTC 2013',
      'helpfulVotes': '39',
      'overallRating': '1',
      'review': "Less than 2 months after purchase it completely stopped working. First it wouldn't detect the pitcher when trying to blend a significant amount, a couple weeks later it wouldn't detect the single serve cup. ",
      'reviewKey': 'b326b0d6-e6ae-4ec5-8080-720f0ad741af',
      'screenName': 'New York',
      'state': 'NY',
      'title': 'Very unhappy',
      'totalComments': '0',
      'totalVotes': '52'
    },
    {
      'city': 'Idaho Falls',
      'customerId': '116317693',
      'datePosted': 'Sun Sep 01 03:18:11 UTC 2013',
      'helpfulVotes': '16',
      'overallRating': '2',
      'review': "This blender is not superior to other smoothie blenders, It doesn't pulverize seeds and leaves green smoothies chunky with a lot of pulp. The single serve concept is amazing, however, my single serve cup began to break right from the start. The prongs became chipped because of the difficulty of screwing it in and out of the base. It won't blend for more than a minute without smelling like burned rubber. While the single serve seemed to blend more smoothly, it didn't hold much, especially when adding ice. I was very disappointed and so I returned it,",
      'reviewKey': '399853f3-4451-40a8-bcd6-bda2d814d9f4',
      'screenName': 'London',
      'state': 'ID',
      'title': 'Very Disappointed',
      'totalComments': '1',
      'totalVotes': '21'
    },
    {
      'city': 'Oakland',
      'customerId': '100025104',
      'datePosted': 'Thu Apr 18 19:42:19 UTC 2013',
      'helpfulVotes': '10',
      'overallRating': '5',
      'review': 'This blender works amazingly, and blends within seconds.  The single serve cups also work really well for smoothies or protein shakes!',
      'reviewKey': 'd602bcdf-53be-4769-94da-3b3fd2517d21',
      'screenName': 'Eric',
      'state': 'CA',
      'title': 'Fantastic Blender',
      'totalComments': '0',
      'totalVotes': '10'
    },
    {
      'city': 'Cambridge',
      'customerId': '172227',
      'datePosted': 'Sat Jan 18 01:20:36 UTC 2014',
      'helpfulVotes': '9',
      'overallRating': '5',
      'review': "I am blown away by this blender. It obliterates ice and frozen fruit - and blends fresh fruits to smooth perfection. It even makes quick work of fresh ginger and tough greens. I did a ton of research before settling on the Ninja. This was a splurge for me - and I spent the extra money to get the single serve cups, thinking I'd take my smoothie to work every morning. But my husband is totally hooked on smoothies now too, so the big pitcher is getting regular use. Tried it out for margaritas tonight... half a lime, half a lemon, half an orange with tequila, honey and ice... unbelievably good. Haven't tried it for soup or sauce yet, but can hardly wait.\n\nI'm impressed by features such as the suction cup feet, the snap-seal lid, and the sensor that prevents the machine from being turned on without the top in place. It cleans up nicely too. \n\nBottom line: I can't stop raving about this thing and have recommended it to all my friends and family.",
      'reviewKey': 'd8e9ac59-6c3a-47be-8b87-f912715ccd18',
      'screenName': 'E',
      'state': 'MA',
      'title': "Couldn't be happier",
      'totalComments': '0',
      'totalVotes': '9'
    },
  ],
  'consolidatedOverallRating': '4',
  'totalReviews': '14'
};

describe('ReviewBox', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources(PLATFORM.moduleName('resources/elements/review-box'))
      .inView('<review-box reviews.bind="customerReview"></review-box>')
      .boundTo({ customerReview: customerReviews });
  });

  it('reviewCount should return number of reviews', done => {
    component.create(bootstrap).then(() => {
      expect(component.viewModel.reviewCount).toBe(customerReviews.totalReviews);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('overallRating should return the consolidatedOverallRating', done => {
    component.create(bootstrap).then(() => {
      expect(component.viewModel.overallRating).toBe(customerReviews.consolidatedOverallRating);
      done();
    }).catch(err => console.log(err.toString()));
  })

  afterEach(() => component.dispose());
});
