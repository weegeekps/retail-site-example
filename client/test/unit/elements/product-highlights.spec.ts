import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';

const inputFeatures = [
  '<strong>Wattage Output:</strong> 1100 Watts',
  '<strong>Number of Speeds:</strong> 3 ',
  '<strong>Capacity (volume):</strong> 72.0 Oz.',
  '<strong>Appliance Capabilities:</strong> Blends',
  '<strong>Includes:</strong> Travel Lid',
  '<strong>Material:</strong> Plastic',
  '<strong>Finish:</strong> Painted',
  '<strong>Metal Finish:</strong> Chrome',
  '<strong>Safety and Security Features:</strong> Non-Slip Base',
  '<strong>Care and Cleaning:</strong> Easy-To-Clean, Dishwasher Safe Parts'
];

const outputFeatures = [
  'Wattage Output: 1100 Watts',
  'Number of Speeds: 3 ',
  'Capacity (volume): 72.0 Oz.',
  'Appliance Capabilities: Blends',
  'Includes: Travel Lid',
  'Material: Plastic',
  'Finish: Painted',
  'Metal Finish: Chrome',
  'Safety and Security Features: Non-Slip Base',
  'Care and Cleaning: Easy-To-Clean, Dishwasher Safe Parts'
];

describe('ProductHighlights', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources(PLATFORM.moduleName('resources/elements/product-highlights'))
      .inView('<product-highlights item-features.bind="features"></product-highlights>')
      .boundTo({ features: inputFeatures });
  });

  it('should list all features without html in order', done => {
    component.create(bootstrap).then(() => {
      const elements = document.querySelectorAll('li') as NodeListOf<HTMLLIElement>;
      for (let i = 0; i < elements.length; ++i) {
        expect(elements[i].textContent).toBe(outputFeatures[i]);
      }
      done();
    }).catch(err => console.log(err.toString()));
  });

  afterEach(() => component.dispose());
});
