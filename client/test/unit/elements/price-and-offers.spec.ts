import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';

const offerPriceData = {
  'currencyCode': 'USD',
  'formattedPriceValue': '$139.99',
  'priceQualifier': 'Online Price',
  'priceValue': '13999'
};

const promotionsData = [
  {
    'Description': [
      {
        'legalDisclaimer': 'Offer available online only. Offer applies to purchases of $50 or more of eligible items across all categories. Look for the &quot;SPEND $50: SHIPS FREE&quot; logo on eligible items. Some exclusions apply. Items that are not eligible are subject to shipping charges. $50 purchase is based on eligible merchandise subtotal. Items that are not eligible, GiftCards, e-GiftCards, gift wrap, tax and shipping and handling charges will not be included in determining merchandise subtotal. Offer valid for orders shipping within the 48 contiguous states, as well as APO/FPO and for Standard and To the Door shipping methods only. Not valid on previous orders.',
        'shortDescription': 'SPEND $50, GET FREE SHIPPING'
      }
    ],
    'endDate': '2014-05-25 06:59:00.001',
    'promotionIdentifier': '10736506',
    'promotionType': 'Buy catalog entries from category X, get shipping at a fixed price',
    'startDate': '2014-05-18 07:00:00.001'
  },
  {
    'Description': [
      {
        'legalDisclaimer': 'Receive a $25 gift card when you buy a Ninja Professional Blender with single serve blending cups or a Ninja MEGA Kitchen System. Not valid on previous orders. On your order summary, the item subtotal will reflect the price of the qualifying item plus the amount of the free gift card, followed by a discount given for the amount of the free gift card. &nbsp;Your price on the order summary will be the price of the qualifying item (the total charges for the qualifying item and gift card). &nbsp;Your account will actually be charged the amount of the qualifying item reduced by the amount of the gift card, and a separate charge for the amount of the gift card. The gift card will be sent to the same address as your order and will ship separately. If you want to return the item you purchased to a Target Store, you may either keep the gift card and just return the qualifying item (you will be refunded the amount of the qualifying item reduced by the amount of the gift card), or you can return the qualifying item and the gift card &nbsp;for a full refund using the online receipt. If you return the item you purchased by mail, keep the gift card; you will be refunded the amount of the qualifying item reduced by the amount of the gift card. Offer expires 05/24/14 at 11:59pm PST.',
        'shortDescription': '$25 gift card with purchase of a select Ninja Blender'
      }
    ],
    'endDate': '2014-05-25 06:59:00.001',
    'promotionIdentifier': '10730501',
    'promotionType': 'Multiple Items Free Gift',
    'startDate': '2014-05-11 07:00:00.001'
  }
];

describe('PriceAndOffers', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources(PLATFORM.moduleName('resources/elements/price-and-offers'))
      .inView('<price-and-offers price.bind="offerPrice" promotions.bind="promotions"></price-and-offers>')
      .boundTo({ offerPrice: offerPriceData, promotions: promotionsData });
  });

  it('should show the price value', done => {
    component.create(bootstrap).then(() => {
      const element = document.querySelector('.price-value') as HTMLDivElement;
      expect(element.textContent).toBe(offerPriceData.formattedPriceValue);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should show the price qualifier', done => {
    component.create(bootstrap).then(() => {
      const element = document.querySelector('.price-qualifier') as HTMLDivElement;
      expect(element.textContent).toBe(offerPriceData.priceQualifier);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should show the promotions', done => {
    component.create(bootstrap).then(() => {
      const elements = document.querySelectorAll('.offers-item') as NodeListOf<HTMLDivElement>;
      for (let i = 0; i < elements.length; ++i) {
        expect(elements[i].textContent).toBe(promotionsData[i].Description[0].shortDescription);
      }
      done();
    }).catch(err => console.log(err.toString()));
  });

  afterEach(() => component.dispose());
});
