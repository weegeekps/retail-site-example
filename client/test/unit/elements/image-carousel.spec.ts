import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';

const imagesData = {
  'AlternateImages': [
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758_Alt01'
    },
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758_Alt02'
    },
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758_Alt03'
    },
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758_Alt04'
    },
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758_Alt05'
    },
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758_Alt06'
    },
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758_Alt07'
    }
  ],
  'PrimaryImage': [
    {
      'image': 'http:\/\/target.scene7.com\/is\/image\/Target\/14263758'
    }
  ],

  'imageCount': '8',
  'source': 'internal'
};

describe('ImageCarousel', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources(PLATFORM.moduleName('resources/elements/image-carousel'))
      .inView('<image-carousel image-collection.bind="images"></image-carousel>')
      .boundTo({ images: imagesData });
  });

  it('should render the primary image', done => {
    component.create(bootstrap).then(() => {
      const imageElement = document.querySelector('.image-carousel-image') as HTMLImageElement;
      expect(imageElement.src).toBe(imagesData.PrimaryImage[0].image);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should render the left thumbnail', done => {
    component.create(bootstrap).then(() => {
      const imageElement = document.querySelector('.image-carousel-selector-thumbnail-left') as HTMLImageElement;
      expect(imageElement.src).toBe(imagesData.AlternateImages[2].image);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should render the center thumbnail', done => {
    component.create(bootstrap).then(() => {
      const imageElement = document.querySelector('.image-carousel-selector-thumbnail-center') as HTMLImageElement;
      expect(imageElement.src).toBe(imagesData.PrimaryImage[0].image);
      done()
    }).catch(err => console.log(err.toString()));
  })

  it('should render the right thumbnail', done => {
    component.create(bootstrap).then(() => {
      const imageElement = document.querySelector('.image-carousel-selector-thumbnail-right') as HTMLImageElement;
      expect(imageElement.src).toBe(imagesData.AlternateImages[3].image);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('previousImage() subtracts index by 1', done => {
    component.create(bootstrap).then(() => {
      const startingIndex = component.viewModel.imageIndex;
      component.viewModel.previousImage();
      expect(component.viewModel.imageIndex).toBe(startingIndex - 1);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('nextImage() adds index by 1', done => {
    component.create(bootstrap).then(() => {
      const startingIndex = component.viewModel.imageIndex;
      component.viewModel.nextImage();
      expect(component.viewModel.imageIndex).toBe(startingIndex + 1);
      done();
    }).catch(err => console.log(err.toString()));
  });

  afterEach(() => component.dispose());
});
