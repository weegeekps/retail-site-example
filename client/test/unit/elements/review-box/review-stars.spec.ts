import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';

const reviewRating = 3;

describe('ReviewStars', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources(PLATFORM.moduleName('resources/elements/review-box/review-stars'))
      .inView('<review-stars review-score.bind="reviewScore"></review-stars>')
      .boundTo({ reviewScore: reviewRating });
  });

  it('should highlight the correct number of stars', done => {
    component.create(bootstrap).then(() => {
      const elements = document.querySelectorAll('.red-star') as NodeListOf<HTMLDivElement>;
      expect(elements.length).toBe(reviewRating);
      done();
    }).catch(err => console.log(err.toString()));
  });

  // Testing signalers is currently not well documented. Need to ask on the gitter after vacation.

  afterEach(() => component.dispose());
});
