import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';

const review = {
  'datePosted': 'Thu Apr 18 19:42:19 UTC 2013', // Correct formatting already tested.
  'overallRating': '5',
  'review': 'This blender works amazingly, and blends within seconds.  The single serve cups also work really well for smoothies or protein shakes!',
  'screenName': 'Eric',
  'title': 'Fantastic Blender'
};

describe('ReviewBlock', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources(PLATFORM.moduleName('resources/elements/review-box/review-block'))
      .inView('<review-block review.bind="review"></review-block>')
      .boundTo({ review: review });
  });

  it('should show the correct title', done => {
    component.create(bootstrap).then(() => {
      const element = document.querySelector('.review-block-title') as HTMLHeadingElement;
      expect(element.textContent.trim()).toBe(review.title);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should show the correct screen name', done => {
    component.create(bootstrap).then(() => {
      const element = document.querySelector('a') as HTMLAnchorElement;
      expect(element.textContent.trim()).toBe(review.screenName);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should show the review', done => {
    component.create(bootstrap).then(() => {
      const element = document.querySelector('.review-block-text') as HTMLParagraphElement;
      expect(element.textContent.trim()).toBe(review.review);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should show something for date', done => {
    component.create(bootstrap).then(() => {
      const element = document.querySelector('.review-block-author-date') as HTMLSpanElement;
      expect(element.textContent.trim()).toBeTruthy();
      done();
    }).catch(err => console.log(err.toString()));
  });

  afterEach(() => component.dispose());
});
