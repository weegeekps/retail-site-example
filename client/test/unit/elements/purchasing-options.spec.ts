import { StageComponent } from 'aurelia-testing';
import { bootstrap } from 'aurelia-bootstrapper';
import { PLATFORM } from 'aurelia-pal';
import { PurchasingOptions } from 'resources/elements/purchasing-options';

const catalogEntry = {
  'ReturnPolicy': [
    {
      'ReturnPolicyDetails': [
        {
          'guestMessage': 'View our return policy',
          'policyDays': '100',
          'user': 'Regular Guest'
        },
        {
          'guestMessage': 'View our return policy',
          'policyDays': '120',
          'user': 'Best Guest'
        }
      ],
      'legalCopy': 'refund/exchange policy'
    }
  ],
  'purchasingChannelCode': '0'
};

describe('PurchasingOptions', () => {
  let component;

  beforeEach(() => {
    component = StageComponent
      .withResources(PLATFORM.moduleName('resources/elements/purchasing-options'))
      .inView('<purchasing-options catalog-entry.bind="catalogEntry"></purchasing-options>')
      .boundTo({ catalogEntry: catalogEntry });
  });

  it('should show quantity', done => {
    component.create(bootstrap).then(() => {
      const quantityElement = document.querySelector('.quantity-selector-count') as HTMLDivElement;
      expect(+quantityElement.textContent).toBe(component.viewModel.quantity);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should show correct return policy num days for users', done => {
    component.create(bootstrap).then(() => {
      expect(component.viewModel.returnPolicyNumDaysForUser).toBe(+catalogEntry.ReturnPolicy[0].ReturnPolicyDetails[0].policyDays);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should return true for both showAddToCartButton and showPurchaseInStoreButton if purchasingChannelCode is 0', done => {
    component.create(bootstrap).then(() => {
      component.viewModel.catalogEntry.purchasingChannelCode = 0;
      expect(component.viewModel.showAddToCartButton).toBeTruthy();
      expect(component.viewModel.showPurchaseInStoreButton).toBeTruthy();
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should return true for showAddToCartButton if purchasingChannelCode is 1', done => {
    component.create(bootstrap).then(() => {
      component.viewModel.catalogEntry.purchasingChannelCode = 1;
      expect(component.viewModel.showAddToCartButton).toBeTruthy();
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should return false for showAddToCartButton if purchasingChannelCode is 2', done => {
    component.create(bootstrap).then(() => {
      component.viewModel.catalogEntry.purchasingChannelCode = 2;
      expect(component.viewModel.showAddToCartButton).toBeFalsy();
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should return true for showPurchaseInStoreButton if purchasingChannelCode is 2', done => {
    component.create(bootstrap).then(() => {
      component.viewModel.catalogEntry.purchasingChannelCode = 2;
      expect(component.viewModel.showPurchaseInStoreButton).toBeTruthy();
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('should return false for showPurchaseInStoreButton if purchasingChannelCode is 1', done => {
    component.create(bootstrap).then(() => {
      component.viewModel.catalogEntry.purchasingChannelCode = 1;
      expect(component.viewModel.showPurchaseInStoreButton).toBeFalsy();
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('addQuantity adds 1 to quantity', done => {
    component.create(bootstrap).then(() => {
      component.viewModel.quanitity = 1;
      component.viewModel.addQuantity();
      expect(component.viewModel.quantity).toBe(2);
      done();
    }).catch(err => console.log(err.toString()));
  });

  it('subtractQuantity subtracts 1 from quantity', done => {
    component.create(bootstrap).then(() => {
      component.viewModel.quantity = 5;
      component.viewModel.subtractQuantity();
      expect(component.viewModel.quantity).toBe(4);
      done();
    }).catch(err => console.log(err.toString()));
  });

  afterEach(() => component.dispose());
});
