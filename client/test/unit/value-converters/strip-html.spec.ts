import { StripHtmlValueConverter } from 'resources/value-converters/strip-html';

const testInput = 'refund/exchange policy<br/><br/><p style=\"font-size:13px;\">';
const testExpected = 'refund/exchange policy';

describe('StripHtmlValueConverter', () => {
  let valueConverter;

  beforeEach(() => {
    valueConverter = new StripHtmlValueConverter();
  });

  it('should strip html tags', done => {
    const converted = valueConverter.toView(testInput);
    expect(converted).toBe(testExpected);
    done();
  });
});
