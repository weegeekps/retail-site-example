import { DateValueConverter } from 'resources/value-converters/date';

const testInput = 'Thu Apr 18 19:42:19 UTC 2013';
const testExpected = 'April 18, 2013';

describe('DateValueConverter', () => {
  let valueConverter;

  beforeEach(() => {
    valueConverter = new DateValueConverter();
  });

  it('should convert date', done => {
    const converted = valueConverter.toView(testInput);
    expect(converted).toBe(testExpected);
    done();
  });
});
