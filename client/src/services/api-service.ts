import { autoinject } from 'aurelia-framework';
import { HttpClient } from 'aurelia-fetch-client';
import { ICatalogEntry } from 'models/catalog-entry';

@autoinject
export default class ApiService {
  constructor(private httpClient: HttpClient) {
    this.httpClient.configure(config => {
      config
        .withBaseUrl('api/v1');
    });
  }

  /**
   * Get the catalog entry from the server.
   * 
   * @param itemId The Item Id for the Catalog Entry.
   */
  getCatalogEntry(itemId: string): Promise<ICatalogEntry> {
    const url = `/catalog-entry/${itemId}`;

    return this.httpClient.fetch(url)
      .then(response => response.json())
      .then(response => response.CatalogEntryView[0]) // Return the single CatalogEntry.
      .catch(this.handleError.bind(this, url));
  }

  /**
   * Handles errors on API calls.
   * 
   * @param err Error object.
   * @param url The URL we were attempting to access.
   */
  private handleError(err, url) {
    console.error(`Could not retrieve: ${url}`);

    // If this were a real application, we'd present the user with a error message now.
  }
}
