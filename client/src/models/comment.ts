export interface IComment {
  city: string,
  commentKey: string,
  commentText: string,
  postedDate: string,
  screenName: string,
  userKey: string,
  userTier: string,
}
