export interface IItemDescription {
  /**
   * Features are slash-escaped HTML.
   */
  features: string[];
}
