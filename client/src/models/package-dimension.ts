export interface IPackageDimension {
  name: string,
  unit: string,
  value: string,
}
