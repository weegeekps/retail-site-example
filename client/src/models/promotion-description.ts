export interface IPromotionDescription {
  legalDisclaimer: string,
  shortDescription: string,
}
