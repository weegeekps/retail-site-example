import { IOfferPrice } from "./offer-price";

export interface IOfferCollection {
  OfferPrice: IOfferPrice[],
}
