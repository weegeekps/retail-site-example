export interface IImage {
  /**
   * Needs to be unescaped.
   */
  image: string,
}
