import { IImage } from './image';

export interface IImageCollection {
  AlternateImages: IImage[],
  PrimaryImage: IImage[],
  imageCount: string,
  source: string,
}
