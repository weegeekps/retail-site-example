import { IReturnPolicyDetails } from "./return-policy-details";

export interface IReturnPolicy {
  ReturnPolicyDetails: IReturnPolicyDetails[],
  legalCopy: string,
}
