import { IPromotionDescription } from "./promotion-description";

export interface IPromotion {
  Description: IPromotionDescription[],
  endDate: string,
  promotionIdentifier: string,
  promotionType: string,
  startDate: string,
}
