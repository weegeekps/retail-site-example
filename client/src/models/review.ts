import { IComment } from './comment';
import { IRatableAttribute } from './ratable-attribute';

export interface IReview {
  RatableAttributes: IRatableAttribute[],
  Comments?: IComment[],
  city?: string,
  customerId?: string,
  datePosted: string,
  helpfulVotes?: string,
  overallRating: string,
  review: string,
  reviewKey: string,
  screenName: string,
  state?: string,
  title: string,
  totalComments?: string,
  totalVotes?: string,
}
