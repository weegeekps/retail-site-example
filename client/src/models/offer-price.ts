export interface IOfferPrice {
  currencyCode: string,
  formattedPriceValue: string,
  priceQualifier: string,
  priceValue: string,
}
