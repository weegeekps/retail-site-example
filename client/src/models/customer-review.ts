import { IRatableAttribute } from "./ratable-attribute";
import { IReview } from "./review";

export interface ICustomerReview {
  Con: IReview,
  ConsolidatedRatableAttributes: IRatableAttribute[],
  Pro: IReview,
  Reviews: IReview[],
  totalPages: string,
  totalReviews: string,
  consolidatedOverallRating: string,
}
