export interface IRatableAttribute {
  description: string,
  name: string,
  value: string,
}
