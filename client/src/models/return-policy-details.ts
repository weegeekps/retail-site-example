export interface IReturnPolicyDetails {
  guestMessage: string,
  policyDays: string,
  user: string,
}
