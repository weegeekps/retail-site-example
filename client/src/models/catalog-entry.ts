import { ICustomerReview } from './customer-review';
import { IImageCollection } from './image-collection';
import { IItemDescription } from './item-description';
import { IOfferCollection } from './offer-collection';
import { IPackageDimension } from './package-dimension';
import { IPromotion } from './promotion';
import { IReturnPolicy } from './return-policy';

// Everything. Absolutely Everything, is a string in the JSON.
export interface ICatalogEntry {
  CustomerReview: ICustomerReview[],
  DPCI: string,
  Images: IImageCollection[],
  ItemDescription: IItemDescription[],
  Offers: IOfferCollection[],
  POBoxProhibited: string,
  PackageDimension: IPackageDimension[],
  Promotions: IPromotion[],
  ReturnPolicy: IReturnPolicy[],
  UPC: string,
  applyCouponLink: string,
  buyable: string,
  callOutMsg: string,
  classId: string,
  department: string,
  eligibleFor: string,
  inventoryCode: string,
  inventoryStatus: string,
  itemId: string,
  itemType: string,
  manufacturer: string,
  manufacturerPartNumber: string,
  packageQuantity: string,
  partNumber: string,
  purchasingChannel: string,
  purchasingChannelCode: string,
  shortDescription: string,
  title: string,
  webclass: string,
}
