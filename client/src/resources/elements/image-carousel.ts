import { bindable, computedFrom } from 'aurelia-framework';
import { IImageCollection } from 'models/image-collection';
import { IImage } from 'models/image';

export class ImageCarousel {
  @bindable imageCollection: IImageCollection;

  imageLocations: string[];
  imageIndex: number; 

  imageCollectionChanged(newValue) {
    this.imageLocations = this.imageCollection.AlternateImages.map(i => i.image);

    // Put the Primary Image in the middle like on the image provided.
    this.imageIndex = Math.floor(this.imageLocations.length / 2);
    this.imageLocations.splice(this.imageIndex, 0, this.imageCollection.PrimaryImage[0].image);
  }

  nextImage() {
    if (this.imageIndex + 1 > this.imageLocations.length) {
      return;
    }

    this.imageIndex++;
  }

  previousImage() {
    if (this.imageIndex - 1 < 0) {
      return;
    }

    this.imageIndex--;
  }

  @computedFrom('imageIndex')
  get currentImage() {
    if (!this.imageLocations) {
      return;
    }

    return this.imageLocations[this.imageIndex];
  }

  @computedFrom('imageIndex')
  get currentThumbnail() {
    if (!this.imageLocations) {
      return;
    }

    return this.imageLocations[this.imageIndex];
  }

  @computedFrom('imageIndex')
  get leftThumbnail() {
    if (!this.imageLocations || this.imageIndex - 1 < 0) {
      return;
    }

    return this.imageLocations[this.imageIndex - 1];
  }

  @computedFrom('imageIndex')
  get rightThumbnail() {
    if (!this.imageLocations || this.imageIndex + 1 > this.imageLocations.length) {
      return;
    }

    return this.imageLocations[this.imageIndex + 1];
  }
}

