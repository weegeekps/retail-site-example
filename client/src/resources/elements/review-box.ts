import { bindable, computedFrom } from 'aurelia-framework';
import { ICustomerReview } from 'models/customer-review';

export class ReviewBox {
  @bindable reviews: ICustomerReview;

  @computedFrom('reviews')
  get reviewCount() {
    if (!this.reviews) {
      return 0;
    }

    return this.reviews.totalReviews;
  }

  @computedFrom('reviews')
  get overallRating() {
    if (!this.reviews) {
      return 0;
    }

    return this.reviews.consolidatedOverallRating;
  }
}

