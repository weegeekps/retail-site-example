import { bindable, autoinject } from 'aurelia-framework';
import { BindingSignaler } from 'aurelia-templating-resources';

@autoinject
export class ReviewStars {
  @bindable reviewScore: number;
  @bindable largeStars = false;

  constructor(private signaler: BindingSignaler) { }

  reviewScoreChanged() {
    this.signaler.signal('refresh-stars');
  }

  /**
   * Returns the color star for the index value of the repeater in review-stars.html.
   * 
   * @param index Repeater index value.
   */
  computeStarColor(index: number) {
    if (!this.reviewScore) {
      return 'gray-star';
    }

    return index < this.reviewScore ? // if
      'red-star' : // true
      'gray-star'; // false
  }
}

