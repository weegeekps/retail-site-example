import {bindable} from 'aurelia-framework';

export class ProductHighlights {
  @bindable itemFeatures: string[];
}

