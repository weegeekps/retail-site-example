import { bindable } from 'aurelia-framework';
import { IOfferPrice } from 'models/offer-price';
import { IPromotion } from 'models/promotion';

export class PriceAndOffers {
  @bindable price: IOfferPrice;
  @bindable promotions: IPromotion[];
}

