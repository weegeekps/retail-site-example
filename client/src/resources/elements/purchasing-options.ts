import { bindable, bindingMode, computedFrom } from 'aurelia-framework';
import { ICatalogEntry } from 'models/catalog-entry';

export class PurchasingOptions {
  // In a real ecommerce application, we would generally want the entire catalog entry here so we have all
  //   relevant data for making the call to add the product (and quantity) to the cart. I've simulated that here.
  @bindable catalogEntry: ICatalogEntry;

  quantity = 1;

  addQuantity(): void {
    this.quantity++;
  }

  subtractQuantity(): void {
    if (this.quantity === 1) {
      return;
    }

    this.quantity--;
  }

  @computedFrom('catalogEntry')
  get showAddToCartButton(): boolean {
    if (!this.catalogEntry) {
      return false;
    }

    return +this.catalogEntry.purchasingChannelCode === 0 || +this.catalogEntry.purchasingChannelCode === 1;
  }

  @computedFrom('catalogEntry')
  get showPurchaseInStoreButton(): boolean {
    if (!this.catalogEntry) {
      return false;
    }

    return +this.catalogEntry.purchasingChannelCode === 0 || +this.catalogEntry.purchasingChannelCode === 2;
  }

  @computedFrom('catalogEntry')
  get returnPolicyNumDaysForUser(): number {
    if (!this.catalogEntry) {
      return 0;
    }

    // Imagine there is code here to get the user info. For now we're just saying "Regular User".
    return +this.catalogEntry.ReturnPolicy[0].ReturnPolicyDetails.find(policy => policy.user === 'Regular Guest').policyDays;
  }
}

