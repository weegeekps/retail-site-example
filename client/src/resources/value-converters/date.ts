import * as moment from 'moment';

// Mon Mar 11 13:13:55 UTC 2013
const INPUT_DATE_FORMAT = 'ddd MMM D HH:mm:ss Z YYYY';
const OUTPUT_DATE_FORMAT = 'MMMM D, YYYY';

export class DateValueConverter {
  toView(value) {
    return moment.utc(value, INPUT_DATE_FORMAT).format(OUTPUT_DATE_FORMAT);
  }
}

