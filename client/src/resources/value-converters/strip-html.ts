export class StripHtmlValueConverter {
  toView(value) {
    // This uses the browser's built in DOMParser in order to strip all HTML tags from a string.
    //   In this case, this is necessary to clean up the data in item-data.json. There appear to be
    //   several strong tags in the product highlights that are ignored in the images given.
    const shadowDocument = new DOMParser().parseFromString(value, 'text/html');
    return shadowDocument.body.textContent || '';
  }
}

