import '../styles/app.scss';
import { autoinject } from 'aurelia-framework';
import ApiService from 'services/api-service';
import { ICatalogEntry } from 'models/catalog-entry';

@autoinject
export class App {
  private catalogEntry: ICatalogEntry;
  
  constructor(private apiService: ApiService) { }

  attached() {
    // A note for the reviewer. Typically, and especially with data that will mutate, I would use a store backed by immutable.js
    //  to handle getting the data through the ApiService that works as a single state of truth, similar to Redux. With
    //  Aurelia this is not required, but it helps with larger apps in managing the state across multiple components. 
    //  I opted to not do that here in the interest of time.
    this.apiService.getCatalogEntry('1234567')
      .then(ce => this.catalogEntry = ce);
  }
}
