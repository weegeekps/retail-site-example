# Retail Site Example

This repository needs at least Node.js 8.11.2 to run.

## Basic Instructions

To run locally using just the production build and the node application, follow these instructions after cloning the repository.

1. Use `npm` or `yarn` in the `/server` directory to install all server dependencies. Either is fine, both `npm-shrinkwrap.json` and `yarn.lock` are present. `yarn` was used to develop this example.
2. Run `npm run start` or `yarn run start`.
3. Open a browser to http://localhost:3000 to view the example site.

## Dev Build & Unit Test Instructions

To run either the dev build or the karma unit tests, follow these instructions after cloning the repository.

_Note: You can install the Aurelia CLI locally if you want, or you can just use `npx`._

1. Follow the basic instructions above; the server is used to serve the item-data.json file to the client.
2. Use `npm` or `yarn` in the `/client` directory to install all client dependencies.
3. To run the dev build, run `npx au run --watch`. This will start the Webpack Dev Server using the Aurelia CLI accessible at http://localhost:8080. The API is proxied through the WDS.
4. To run Karma and the Unit Tests, run `npx au karma --watch`. This will start Karma using Chrome and run the unit tests.
5. To run the prod build, run `npx au build --env prod`.

## Special Notes for Reviewers

* The NodeJS server is a very simple Express app. It serves the static content from `/client/dist` and the `item-data.json` data through a single api endpoint.
* If you are unfamiliar with Aurelia, it is a declarative, component-based framework by Rob Eisenberg. I chose this framework due to the fact that I've been immersed in the Aurelia community since before it's 1.0 release in 2016. It's extremely powerful yet unopinionated and provides a great canvas for producing performant web apps using a variety of design patterns, including Flux, Reactive Programming (via Rx.js or other tools), or even classic MVVM architecture. Aurelia and React are currently my two favorite frameworks to use, with Aurelia beating out React slightly.
* Normally, I would not check the `/client/dist` folder into Git. In this case it is for the ease of any reviewer of this example site, so that step isn't required to view the example.
* This is using the latest Aurelia CLI with Webpack 4 and TypeScript. TSLint settings by default for Aurelia CLI builds are very forgiving and favor treating TypeScript as an extension of JavaScript rather than a replacement.
* I used Zurb Foundation 6 Sites as a SASS framework. This isn't necessary, but I've found using limited parts of Foundation tends to help cover all bases when dealing with a variety of web browsers.
* Due to very limited time to work on this, some things are not unit tested (Integration tests for the service & tests for the basic App container), and E2E tests unfortunately did not make the cut. Aurelia CLI uses Protractor and Selenium by default, and I did not have sufficient time to properly set up and configure those tools for this project.